<?php
    // Timezone
    date_default_timezone_set('America/Sao_Paulo');
    // Estruturação da Classe Agenda
    class AgendaContato {
        // Atributos
        protected $id;
        public $nome;
        public $email;
        public $dataNasc;
        private $fone;
        public $endereco;
        public $bairro;
        public $cidade;
        public $uf;
        private $status;
        public $referencia;
        public $local;
        public $data;
        public $hora;
        public $dataAtual;
        public $horaAtual;
        // Métodos
        public function agendarCompromisso($r, $l, $d, $h) {
            $this->dataAtual = date('m/d/Y', strtotime($this->dataAtual));
            $this->horaAtual = date('H:i:s');

            if(!empty($r) && !empty($l) && !empty($d) && !empty($h)) {
                $this->setStatus(true);
                //$d->date('m/d/Y', strtotime($this->data));
                if($this->getStatus()) {
                    echo "<p>Compromisso agendado com sucesso!</p>";

                    $d = new DateTime('16:00');
                    $d->sub(new DateInterval('PT1H'));
                    echo $d->format('H:i:s') . "\n";

                    //$d->sub(new DateInterval('PT1H'));

                    /*if($h->sub(new DateInterval('PT1H'))){
                        echo "<p>ATENÇÃO: Falta apenas 1H para o seu compromisso. Não se atrase!</p>";
                    }
                    // se data dia igual data informada avisar contato sobre o agendamento
                    elseif($this->dataAtual == $d && $this->horaAtual == $h) {
                        echo "<p>Obrigado pelo comparecimento. Nós da(o) ".$this->getReferencia()." agradecemos a preferência.</p>";
                        $this->setStatus(false);
                    }*/
                }
            } else {
                echo "<p>Ops! Não foi possível agendar, preencha todos os campos!</p>";
                $this->setStatus(false);
            }
        }
        public function avisarAniver() {
            if($this->getDataNasc() == $this->getDataAtual()) {
                echo "<p>Parabéns ".$this->getNome()."! Feliz aniversário!!</p>";
            }
        }
        // Métodos Especiais
        public function __construct() {
            //$this->setSaldo(0);
            $this->setStatus(false);
        }

        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        }

        public function getNome() {
            return $this->nome;
        }

        public function setNome($nome) {
            $this->nome = $nome;
        }

        public function getEmail() {
            return $this->email;
        }

        public function setEmail($email) {
            $this->email = $email;
        }

        public function getDataNasc() {
            return $this->dataNasc;
        }

        public function setDataNasc($dataNasc) {
            $this->dataNasc = $dataNasc;
        }

        public function getFone() {
            return $this->fone;
        }

        public function setFone($fone) {
            $this->fone = $fone;
        }

        public function getEndereco() {
            return $this->endereco;
        }

        public function setEndereco($endereco) {
            $this->endereco = $endereco;
        }

        public function getBairro() {
            return $this->bairro;
        }

        public function setBairro($bairro) {
            $this->bairro = $bairro;
        }

        public function getCidade() {
            return $this->cidade;
        }

        public function setCidade($cidade) {
            $this->cidade = $cidade;
        }

        public function getUf() {
            return $this->uf;
        }

        public function setUf($uf) {
            $this->uf = $uf;
        }

        public function getStatus() {
            return $this->status;
        }

        public function setStatus($status) {
            $this->status = $status;
        }

        public function getReferencia() {
            return $this->referencia;
        }

        public function setReferencia($referencia) {
            $this->referencia = $referencia;
        }

        public function getLocal() {
            return $this->local;
        }

        public function setLocal($local) {
            $this->local = $local;
        }

        public function getData() {
            return $this->data;
        }

        public function setData($data) {
            $this->data = $data;
        }

        public function getHora() {
            return $this->hora;
        }

        public function setHora($hora) {
            $this->hora = $hora;
        }

        public function getDataAtual() {
            return $this->dataAtual;
        }

        public function setDataAtual($dataAtual) {
            $this->dataAtual = $dataAtual;
        }

        public function getHoraAtual() {
            return $this->horaAtual;
        }

        public function setHoraAtual($horaAtual) {
            $this->horaAtual = $horaAtual;
        }

    }
?>