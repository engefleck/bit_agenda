<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Agenda de Contatos</title>
    </head>
    <body>
        <pre>
        <?php
            require_once 'classes/AgendaContato.php';
            $c1 = new AgendaContato();

            $c1->agendarCompromisso('Engeplus', 'Rua Urussanga, 528 - Vera Cruz - Criciúma/SC', '15/03/2020', '21:00');
            
            print_r($c1);
        ?>
        </pre>
    </body>
</html>